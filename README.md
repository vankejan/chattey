# Chattey
Simple chat single page app built using Socket.io Node.js back-end, express web app framework and plain ES06 client application
and SASS for styling the app

> The socket server is more of a mock-up then real useful implementation

> Client app is present in the [client folder](https://gitlab.fel.cvut.cz/vankejan/chattey/tree/master/client)

## Contents

 - [How to run](#how-to-run)
 - [Using the application](#using-the-app)
 - ["Live" example](#live-example) 
 - [Application features](#features)
 - [Not implemented](#not-implemented)
 - [Implemented](#implemeted)



----
 **External documents:**

 - [Project structure](https://gitlab.fel.cvut.cz/vankejan/chattey/wikis/project-structure)
 - [Extending project](https://gitlab.fel.cvut.cz/vankejan/chattey/wikis/Extending-the-application)



## How to run
### Clone
First clone the repository using:
```
git clone https://github.com/vankejan/chattey.git
```
### Setup
You just need Node.js(+npm) installed in your enviroment.

### Run using npm
Simply run the app from the project folder using:
```
npm run start
```
#### Changing the client and server ports
Then the socket.io server runs on port `3000` and the client app at port `3001`
You can change the ports [here](https://gitlab.fel.cvut.cz/vankejan/chattey/tree/master/server/server.js) 
`port` variable on line 6 for client application

For socket.io server it's [also here](https://gitlab.fel.cvut.cz/vankejan/chattey/tree/master/server/server.js) 
`server.listen(3000, function (err) {` line 60 
and [here](https://gitlab.fel.cvut.cz/vankejan/chattey/tree/master/client/index.html) line 16

## Using the app
**Since the server part is just a mock up, creating new rooms and users doen't work!**

The predefined room codes are: `one`, `two`, `three`

The predefined user names are: `Rick`, `Morty`, `Test` and `Karel`

## Live example
There is a **not fully functional** live example
[Entry page](https://praxis-dolphin-214018.appspot.com) and [Chatroom](https://praxis-dolphin-214018.appspot.com/#/chat/one)
> The client application works fine, but i didn't get the socket.io server running on google app engine

## Features
Chattey provides: 

  - Simple message exchange
  - Chat rooms
  - Sending Google maps using chat
  - Sending files

> Since i was only focusing on the client side, the sending files 
> feature doesn't really send files, but it's just an example of 
> drag and drop file upload.

### UI prototype
#### Entry page
![entry](images/entry.PNG)
#### Chatroom page
![chatroom](images/chat.PNG)
## Not implemented

Things I didn't implement in the application:

![notimplemented](images/notimplemented.PNG)

## Implemeted
Things I (tried to implement?) implemented in the application:

![implemented](images/implemented.PNG)

### HTML5
#### Validita
[W3School Validator](https://validator.w3.org/nu/?doc=https%3A%2F%2Fpraxis-dolphin-214018.appspot.com%2F)
#### Sémantické značky
Example of Chattey DOM structure:

![projectstructure](images/structure.PNG)
#### Grafika SVG/Canvas
Found no real usage of Canvas, but all of the icons are in the form of SVGs

Back button used in project:
```html
<svg id="back-icon" width="13" height="24" viewBox="0 0 13 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M12 1L2 11.56L12 23" stroke="black" stroke-width="2"/>
</svg>
```
or right menu overlay with SVG animation
```html
  <svg id="rightMenuOverlay" width="712" height="1365" viewBox="0 0 660 1365" fill="none" xmlns="http://www.w3.org/2000/svg">
                 <path d="M190.042 1026C334.047 826.007 487.922 -2 0 -2H790V1026H190.042Z" fill="#363636"/>
              ...
                 <text class="chatteyIsTypingText" fill="white" x="390" y="900">Chattey is typing</text>
                 <text class="tripleDot" fill="white" x="622" y="900">.
              ...
                 <text class="tripleDot" fill="white" x="638" y="900">.
                 <animate attributeName="y" from="890" to="900" dur="1.2s" begin="0.4s" repeatCount="indefinite"/></text>
  </svg>
```
#### Média - Audio/Video
I used simple the audio as an easter egg, when you click the three-dots (more) icon in chatroom page,

![dots](images/dots.PNG)

the monkey sound is appended to the page:
```html
<audio autoplay>
            <source src="audio/monkey.wav" type = "audio/wav"/>
            <source src="audio/monkey.mp3" type="audio/mpeg"/>
            Your browser does not support the <code>audio</code> element.
</audio>
```
#### Formulářové prvky
Forms are present mostly in the entry section, where the validation, types and placeholder features are used,
the autofocus property is used in the chat section for the message text area.
### CSS
I used SASS pre-processor for the project, so vendor prefixes were handled by SASS.

[Main SASS file](https://gitlab.fel.cvut.cz/vankejan/chattey/tree/master/client/css/main.scss)
### Javascript
#### OOP přístup
The application is written in ES6 so i used the ES6 Classes.
Nice example is in the [messageHandler.js file](https://gitlab.fel.cvut.cz/vankejan/chattey/tree/master/client/js/pages/handlers/messageHandler.js)
#### Použití pokročilých JS API
I used three of the mentioned APIs:
- Geolocation
- Drag & Drop
- File API
- Clipboard API

#### Geolocation
``` javascript
...
 if ("geolocation" in navigator) {
                /* geolocation is available */
                navigator.geolocation.getCurrentPosition(function(position) {
                    console.log(" found current position : {"+ position.coords.latitude + "," + position.coords.longitude + "}")

                    let pos = {"lat": position.coords.latitude, "lon": position.coords.longitude}

                    field.value = JSON.stringify(pos)
                });
            } else {
                /* geolocation IS NOT available */
                console.log("Geologation not available.")
            }
...
```

#### D&D + File API
[dragAndDropImage function](https://gitlab.fel.cvut.cz/vankejan/chattey/tree/master/client/js/pages/chat.js)
### JS práce s SVG
Another feature implemented in the form of easter egg.
If you manage to click on the bottom part of the right overlay in the entry section text is appended to the SVG.

#### Clipboard API
If you use the Get a link button on the desktop version, the room code gets copied into your clipboard, or logs error if the clipboard API isn't supported.

``` javascript

navigator.permissions.query({name: "clipboard-write"}).then(result => {
        if (result.state == "granted" || result.state == "prompt") {
            this.updateClipboard(this._code)
        }
});
            
            ...
            
 updateClipboard(code) {
    navigator.clipboard.writeText(code).then(function() {
        console.log("Copied room code to the clipboard")
    }, function() {
        /* clipboard write failed */
        console.log("Clipboard write failed")
    });
}

```