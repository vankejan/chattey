import {ParserType} from "../../helpers/enums.js"

// Abstract message
class Message {
    constructor(username, datetime = Date.now()){
        this._username = username
        this._datetime = datetime
        this._messageTemplate = document.createElement("div")
    }

    get getUsername(){
        return this._username
    }

    get getDatetime(){
        return this._datetime
    }

    getFormatedTime(){
        return new Date(this.getDatetime).toLocaleTimeString()
    }

    set setDatetime(datetime){
        this._datetime = datetime
    }

    get getMessageTemplate(){
        return this._messageTemplate;
    }
}

class BasicMessage extends Message{
    constructor(username, datetime = Date.now(), message){
        super(username, datetime);
        this._message = message
        this.buildMessageTemplate()
    }

    get getMessage(){
        return this._message
    }

    buildMessageTemplate(){
        let template_content = /*HTML*/ `
               <div class="messageContent">
                    <p class="messageText">${this.getMessage}</p>
               </div>
                    <p class="messageInfo">${this.getUsername} at ${this.getFormatedTime()}</p>
        `

        this._messageTemplate.innerHTML = template_content
        this._messageTemplate.setAttribute("class", "basicMessage")
    }
}

class EventMessage extends Message{
    constructor(username, datetime = Date.now(), event){
        super(username, datetime)
        this._event = event
        this.buildMessageTemplate()
    }

    get getEvent(){
        return this._event
    }

    buildMessageTemplate(){
        let template_content = /*HTML*/ `
               <div class="messageContent">
                    <p class="messageText">${this.getUsername} ${this.getEvent} at ${this.getFormatedTime()}</p>
               </div>
        `

        this._messageTemplate.innerHTML = template_content
        this._messageTemplate.setAttribute("class", "eventMessage")
    }
}

class MapMessage extends Message{
    constructor(username, datetime = Date.now(), lon, lat){
        super()
        this._username = username
        this._datetime = datetime
        this._messageTemplate = document.createElement("div")
        this._lon = lon
        this._lat = lat
        this.buildMessageTemplate()
    }

    buildMessageTemplate(){
        let template_content = /*HTML*/ `
               <div class="messageContent">
                    <!-- MAP -->
                    <img border="0" src="https://maps.googleapis.com/maps/api/staticmap?center=${this._lat},${this._lon}&zoom=14&size=300x300&key=AIzaSyChvYDFWtfAfosMwTKKsliwgN0yug0JJVU">
               </div>
                    <p class="messageInfo">${this.getUsername} at ${this.getFormatedTime()}</p>
        `


        this._messageTemplate.innerHTML = template_content
        this._messageTemplate.setAttribute("class", "basicMessage")
    }
}

// Factory for creating message object
class MessageFactory {
    constructor(){

    }

    create(data){
        console.log({data})
        if (data.message !== undefined){

            // check for message JsonParser
            if (messageJsonParser(data.message) === ParserType.GEOLOCATION){
                let json = JSON.parse(data.message)
                return new MapMessage(data.user.name, Date.now(), json.lon, json.lat)
            }

            return new BasicMessage(data.user.name, Date.now(), data.message)
        }
        if (data.event !== undefined) {
            return new EventMessage(data.user.name, Date.now(), data.event)
        }
    }
}

// Basic message Send handler
const onSendMessage = (event, chatroom, msg, errorLabel, chatField) => {

    let errors = "";
    
    if (msg.trim() !== "" && msg !== null && msg !== undefined){
        socket.emit('message', {chatroomName: chatroom, message: msg}, (err) => {
            if (err === null || err == undefined) {
                console.log("Sent message" + msg);
            } else {
                console.log("Error sending message " + err)
                errors += err;
                errorLabel.innerText = errors;

                if (err === "select user first"){ // TODO: this is just HOTFIX
                    location.hash = '#/'; // valid -> redirect to chat
                }
            }
        });
    } else{
        console.log("Error sending blank message")
    }

    errorLabel.innerText = errors;
}

// Photo message send handler
const onSendPhotos = (event, chatroom, photos, errorLabel, chatField) => {
    // TODO: Send them messages for real
    console.log("Sending photos")
}

// Message recieve handler
const onRecieveMessage = (data) => {
    console.log("Got message");
    console.log({data});

    return new MessageFactory().create(data)
}

// Parses the json message
const messageJsonParser = (message) => {
    try {
        let json = JSON.parse(message);
        
        if (json.lat !== null || json.lat !== undefined || json.lon !== null || json.lon !== undefined){
            console.log("Message is of type MAP: " + JSON.stringify(json))
            return ParserType.GEOLOCATION
        }
        return null
    } catch (e) {
        console.log("Message not JSON")
        return null
    }
}

export {onSendMessage, onRecieveMessage, onSendPhotos};