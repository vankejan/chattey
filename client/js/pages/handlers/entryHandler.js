import {onConnection} from "./connectionHandler.js"

// ENTER ROOM HANDLER
const entryHandler = (event, nickname, roomName, error) => {
    socket.on('connection', (data) => onConnection(data));

    console.log("Joining room")

    let errs = ""

    // create promise for async connection
    const connectPromise = new Promise(function (resolve, reject) {
        if (nickname === null || roomName === undefined || roomName === null || nickname === undefined) { // blank username or room code
            errs = "Nickname or Room code is not defined."
            console.log(errs)
            reject(errs)
        }

        try {
            socket.emit('register', nickname, (err) => {
                if (err === null || err === undefined || err === '') {
                    console.log("Registered: " + nickname);
                } else { // wrong username (occupied or invalid)
                    errs = err
                    console.log(errs)
                    reject(errs)
                }
            });

            socket.emit('join', roomName, (err) => {
                if (err === null || err === undefined || err === '') {
                    console.log("Joined: " + roomName);
                    resolve(roomName)
                } else {  // wrong room code
                    errs = err
                    console.log(errs)
                    reject(errs)
                }
            });
        } catch (e) {
            console.log(e)
            errs = "No connection, websocket server might be down for a while."
            reject(errs)
        }
    });

    connectPromise.then(
        function(roomName) {
            location.hash = '#/chat/' + roomName; // valid -> redirect to chat
            document.body.removeChild(document.querySelector("a.back-nav"))
        }).catch(
        // Log the rejection reason
        (errs) => {
            console.log("Connecting raised these errors: " + errs)
                error.style.display = "block"
                error.innerText = errs // invalid -> write errors
        });
};

// CREATE ROOM HANDLER
const createHandler = (event, nickname, roomName, error) => {
    console.log("Creating room")

    let errs = ""

    // create promise for async connection
    const connectPromise = new Promise(function (resolve, reject) {
        if (nickname === null || roomName === undefined || roomName === null || nickname === undefined) { // blank username or room name
            errs = "Nickname or Room code is not defined."
            console.log(errs)
            reject(errs)
        }

        try {
            socket.emit('register', nickname, (err) => {
                if (err === null || err === undefined || err === '')  {
                    console.log("Registered: " + nickname);
                } else { // wrong username (occupied or invalid)
                    errs = err
                    console.log(errs)
                    reject(errs)
                }
            });

            socket.emit('createRoom', roomName, (err) => {
                if (err === null || err === undefined || err === '')  {
                    console.log("Joined: " + roomName);
                    resolve(roomName)
                } else {  // wrong room name (occupied or invalid)
                    errs = err
                    console.log(errs)
                    reject(errs)
                }
            });

        } catch (e) {
            console.log(e)
            errs = "No connection, websocket server might be down for a while."
            reject(errs)
        }
    });

    connectPromise.then(
        function(roomName) {
            location.hash = '#/chat/' + roomName; // valid -> redirect to chat
            document.body.removeChild(document.querySelector("a.back-nav"))
        }).catch(
        // Log the rejection reason
        (errs) => {
            error.innerText = errs // invalid -> write errors
        });
};

export {entryHandler, createHandler};