let Error404 = {
    render : async () => {

        let view =  /*html*/`
            <section class="error_section">
                <h1> 404 - nothing here sorry. </h1>
            </section>
        `

        return view
    },
    after_render: async () => {}
}

export default Error404;