import entryModule from "./modules/entryModule.js"
import {entryHandler, createHandler} from "./handlers/entryHandler.js"
import Page from "./page.js"

class Entry extends Page{
    constructor(){
        super();
        // mobile entry section
        this._chooseEntrySection =  entryModule.choose();
        this._gotoEntrySection = entryModule.goto();
        this._createEntrySection = entryModule.create();
        this._backnavEntrySection = entryModule.back();
        this._back = null;
        // desktop and tablet version of EntrySection
        this._desktopEntrySection = entryModule.desktop();
        // easter egg active
        this._easterEgg = false
    }

    // set content of this._view -> entry section content
    setView(entrySection) {
        this._view =  /*html*/`
            <!-- Main logo section-->
            <svg id="main-logo" viewBox="0 0 198 215" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M196.962 196.539L0 154.403L154.179 7.03069L196.962 196.539Z" fill="#77D1AB"/>
                <path d="M177.958 113.716L1.41797 152.946L155.597 5.57277L177.958 113.716Z" fill="#D0FFE9"/>
                <path d="M28.9271 151.125C21.7597 151.787 15.8135 152.117 11.0888 152.117C7.93895 152.117 5.62481 151.356 4.14632 149.837C2.66784 148.317 1.92859 145.937 1.92859 142.699V126.837C1.92859 123.367 2.6839 120.856 4.19454 119.302C5.73731 117.716 8.1961 116.923 11.5709 116.923H28.9271V123.863H13.4994C11.5709 123.863 10.6067 124.854 10.6067 126.837V142.699C10.6067 143.459 10.8156 144.07 11.2334 144.533C11.6834 144.962 12.2459 145.177 12.9208 145.177C13.5958 145.177 14.3351 145.177 15.1386 145.177C15.9421 145.144 16.7778 145.111 17.6456 145.078C18.5134 145.045 19.3812 145.012 20.249 144.979C21.1489 144.946 22.29 144.88 23.672 144.781C25.0863 144.681 26.8379 144.566 28.9271 144.434V151.125ZM40.7446 125.845C43.8623 125.184 46.5943 124.854 48.9406 124.854H51.1101C53.2957 124.854 55.0474 125.531 56.3652 126.886C57.6829 128.241 58.3419 130.042 58.3419 132.289V151.621H49.9048V133.281C49.9048 132.785 49.7441 132.372 49.4227 132.042C49.1013 131.711 48.6996 131.546 48.2174 131.546H46.53C45.3086 131.546 44.1837 131.612 43.1552 131.744C42.1588 131.876 41.3553 131.975 40.7446 132.042V151.621H32.3076V116.923H40.7446V125.845ZM72.7997 145.177C74.5034 145.177 76.4319 144.847 78.5851 144.186V141.212H71.5944C71.1123 141.212 70.7105 141.377 70.3891 141.707C70.0677 142.038 69.907 142.451 69.907 142.947V143.442C69.907 143.938 70.0677 144.351 70.3891 144.681C70.7105 145.012 71.1123 145.177 71.5944 145.177H72.7997ZM63.8805 125.845C69.7624 125.184 75.0656 124.854 79.7904 124.854C81.9762 124.854 83.7274 125.531 85.0455 126.886C86.3635 128.241 87.0221 130.042 87.0221 132.289V151.621H79.5493L79.0672 149.143C77.5563 150.167 75.998 150.927 74.3907 151.423C72.816 151.886 71.4817 152.117 70.3891 152.117H68.7017C66.5161 152.117 64.7644 151.44 63.4466 150.084C62.1288 148.729 61.47 146.929 61.47 144.681V142.451C61.47 140.204 62.1288 138.403 63.4466 137.048C64.7644 135.693 66.5161 135.016 68.7017 135.016H78.5851V133.033C78.5851 132.537 78.4242 132.124 78.103 131.794C77.7818 131.463 77.3798 131.298 76.8977 131.298C74.8083 131.298 72.4941 131.397 69.9552 131.595C67.4482 131.794 65.4233 131.942 63.8805 132.042V125.845ZM109.186 151.621C106.069 151.951 103.015 152.117 100.026 152.117C97.8407 152.117 96.0888 151.44 94.7707 150.084C93.4534 148.729 92.7941 146.929 92.7941 144.681V132.042H89.9014V125.35H92.7941L93.7583 119.402H101.231V125.35H107.258V132.042H101.231V143.442C101.231 143.938 101.392 144.351 101.713 144.681C102.035 145.012 102.436 145.177 102.918 145.177H109.186V151.621ZM129.479 151.621C126.361 151.951 123.308 152.117 120.319 152.117C118.133 152.117 116.381 151.44 115.064 150.084C113.746 148.729 113.087 146.929 113.087 144.681V132.042H110.194V125.35H113.087L114.051 119.402H121.524V125.35H127.55V132.042H121.524V143.442C121.524 143.938 121.684 144.351 122.006 144.681C122.327 145.012 122.729 145.177 123.211 145.177H129.479V151.621ZM142.287 131.05C141.001 131.05 140.358 131.711 140.358 133.033V135.759H148.554V133.033C148.554 131.711 147.911 131.05 146.626 131.05H142.287ZM156.027 151.125C149.823 151.787 144.199 152.117 139.153 152.117C136.967 152.117 135.215 151.44 133.898 150.084C132.58 148.729 131.921 146.929 131.921 144.681V132.785C131.921 130.307 132.596 128.373 133.946 126.986C135.328 125.565 137.225 124.854 139.635 124.854H149.278C151.688 124.854 153.568 125.565 154.918 126.986C156.3 128.373 156.991 130.307 156.991 132.785V141.955H140.358V143.938C140.358 144.434 140.519 144.847 140.84 145.177C141.162 145.507 141.564 145.673 142.046 145.673C145.228 145.673 149.888 145.425 156.027 144.929V151.125ZM167.59 160.543C165.886 160.543 163.798 160.378 161.323 160.048V153.604H165.662C166.883 153.604 167.67 153.257 168.024 152.563C168.41 151.869 168.345 150.894 167.831 149.638L157.948 125.35H166.867L172.893 142.203L177.956 125.35H186.875L177.232 152.612C176.3 155.223 175.095 157.189 173.617 158.511C172.138 159.866 170.129 160.543 167.59 160.543Z" fill="#393A3A"/>
            </svg>

            <!-- GOTO and Create section -->
            <section class="entry-section">
                <label id="errorLabel"></label>
                ${entrySection}
            </section>
            
            <p class="conditions"><a href="#" id="conditions-headline">Terms of use and conditions</a></p>
        `
    }

    // renders the page
    async render() {
        console.log("Rendering entry section")
        if (mobilecheck()){
            this.setView(this._chooseEntrySection)
        }
        else{
            this.setView(this._desktopEntrySection)
        }
        return this._view;
    }

    // appends listeners mostly
    after_render() { // listeners
        console.log("Adding listeners")
        this._content = document.querySelector("#main-content")
        this._showGoto = document.querySelector("#showGoto")
        this._showCreate = document.querySelector("#showCreate")
        this._errorLabel = document.querySelector("#errorLabel")
        this._rightOverlay = document.querySelector("#rightMenuOverlay")


        if (mobilecheck()){
            this._showGoto.addEventListener("click", this.gotoHandler.bind(this))
            this._showCreate.addEventListener("click",  this.createRoomHandler.bind(this))
        }
        else{
            this.appendCreateHandler()
            this.appendGotoHandler()

            // EASTER EGG
            this._rightOverlay.addEventListener("click", evt => {
                if (!this._easterEgg){
                    console.log("Eater egg active")

                    let message = document.createElementNS(svgNS, "text")
                    message.setAttribute("fill", "white")
                    message.setAttribute("x", "440")
                    message.setAttribute("y", "750")
                    message.innerHTML = "Hi there :-)"

                    let sendBy = document.createElementNS(svgNS, "text")
                    sendBy.setAttribute("fill", "white")
                    sendBy.setAttribute("x", "510")
                    sendBy.setAttribute("y", "770")
                    sendBy.innerHTML = "Chattey at 19:21"

                    this._rightOverlay.appendChild(message)
                    this._rightOverlay.appendChild(sendBy)
                    this._easterEgg = true
                }
            })
        }
    }

    // entry section handlers for creating and entering rooms
    appendCreateHandler(){
        const createSend = document.querySelector("#createSend") // Go to room button
        createSend.addEventListener("click", e =>
            createHandler(e,
                document.querySelector("#createUsername").value,
                document.querySelector("#createRoomCode").value,
                this._errorLabel
            ))

    }
    appendGotoHandler(){
        const gotoSend = document.querySelector("#gotoSend") // Go to room button
        gotoSend.addEventListener("click", e =>
            entryHandler(e,
                document.querySelector("#gotoUsername").value,
                document.querySelector("#gotoRoomCode").value,
                this._errorLabel
            ))
    }

    // handler of goto room button
    gotoHandler(e) {
        console.log("Go to handler called")

        this.setView(this._gotoEntrySection)

        this._content.innerHTML = this._view

        // back navigation
        this.appendBackNavToBody()

        this.appendGotoHandler()
    }

    // handler of create room button
    createRoomHandler(e) {
        console.log("Create handler called")

        this.setView(this._createEntrySection)

        this._content.innerHTML = this._view

        // back navigation
        this.appendBackNavToBody()

        this.appendCreateHandler()
    }

    // append back nav to body
    appendBackNavToBody(){
        if (this._back === null){
            // back navigation
            this._back = document.createElement("a")
            this._back.setAttribute("class", "back-nav")
            this._back.innerHTML = this._backnavEntrySection

            document.body.appendChild(this._back)

            this._back.addEventListener("click", this.backHandler.bind(this))
        }
    }

    // remove back nav from body
    removeBackNavFromBody(){
        if (this._back !== null){
            document.body.removeChild(this._back)
            this._back = null
        }
    }

    // back nav handler
    backHandler(e){
        console.log("Back nav handler called")

        this.setView(this._chooseEntrySection)
        this._content.innerHTML = this._view

        this.removeBackNavFromBody()

        this.after_render()
    }
}

export default Entry;