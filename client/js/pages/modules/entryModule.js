let EntryModule = {
    choose(){ // MOBILE ONLY SELECT INPUTS
        return /*html*/`
            <section class="chooseSection">
                <input type="button" id="showGoto" value="Go to chat room">
                <input type="button" id="showCreate" value="Create chat room">  
            </section>
        `
    }, // USED ON MOBILE VERSION ONLY
    goto(){
        let view =  /*html*/`
            <!-- Go to chat room section --> 
            <section class="gotoForm">
                
                <section id="gotoForm-inputs" class="entry-inputs">
                     <!-- URL ICON --> 
                     <svg id="url-icon" width="58" height="58" viewBox="0 0 58 58" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0)">
                        <path d="M29 58C45.0163 58 58 45.0163 58 29C58 12.9837 45.0163 0 29 0C12.9837 0 0 12.9837 0 29C0 45.0163 12.9837 58 29 58Z" 
                        fill="#525252"/>
                        <path d="M19.4154 22.4H22.6554V31.58C22.6554 32.84 22.3674 33.758 21.7905 34.334C21.2274 34.898 20.3154 35.18 19.0554 35.18H14.9054C13.6554 35.18 12.7374 34.898 12.1614 34.334C11.5974 33.758 11.3154 32.84 11.3154 31.58V22.4H14.5554V31.58C14.5554 32.3 14.9054 32.66 15.6354 32.66H18.3354C19.0554 32.66 19.4154 32.3 19.4154 31.58V22.4ZM24.8154 22.4H32.3754C33.6354 22.4 34.5474 22.688 35.1114 23.264C35.6874 23.828 35.9754 24.74 35.9754 26V27.62C35.9754 28.616 35.8074 29.39 35.4714 29.942C35.1474 30.494 34.6254 30.86 33.9054 31.04L36.1554 35H32.6454L30.5754 31.22H28.0554V35H24.8154V22.4ZM32.7354 26C32.7354 25.28 32.3754 24.92 31.6554 24.92H28.0554V28.7H31.6554C32.3754 28.7 32.7354 28.34 32.7354 27.62V26ZM41.0104 32.48H47.5804V35H37.7704V22.4H41.0104V32.48Z"
                        fill="white"/>
                        </g>
                        <defs>
                        <clipPath id="clip0">
                            <rect width="58" height="58" fill="white"/>
                        </clipPath>
                        </defs>
                    </svg>
                    <!-- URL INPUT --> 
                    <input type="text" id="gotoRoomCode" placeholder="Room URL">
                    
                    <!-- PERSON/NICKNAME ICON --> 
                    <svg id="person-icon" width="53" height="53" viewBox="0 0 53 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                       <g clip-path="url(#clip0)">
                        <path d="M18.613 41.552L10.706 45.865C10.242 46.118 9.82501 46.429 9.43701 46.768C14.047 50.655 19.998 53 26.5 53C32.954 53 38.867 50.69 43.464 46.856C43.04 46.498 42.58 46.176 42.07 45.922L33.603 41.689C32.509 41.142 31.818 40.024 31.818 38.801V35.479C32.056 35.208 32.328 34.86 32.619 34.449C33.773 32.819 34.646 31.026 35.251 29.145C36.337 28.81 37.137 27.807 37.137 26.615V23.069C37.137 22.289 36.79 21.592 36.251 21.104V15.978C36.251 15.978 37.304 8.00101 26.501 8.00101C15.698 8.00101 16.751 15.978 16.751 15.978V21.104C16.211 21.592 15.865 22.289 15.865 23.069V26.615C15.865 27.549 16.356 28.371 17.091 28.846C17.977 32.703 20.297 35.479 20.297 35.479V38.719C20.296 39.899 19.65 40.986 18.613 41.552Z" fill="#E7ECED"/>
                        <path d="M26.9529 0.0040072C12.3199 -0.245993 0.253949 11.414 0.00394877 26.047C-0.138051 34.344 3.55995 41.801 9.44795 46.76C9.83295 46.424 10.2459 46.116 10.7049 45.866L18.6119 41.553C19.6489 40.987 20.2949 39.9 20.2949 38.718V35.478C20.2949 35.478 17.9739 32.702 17.0889 28.845C16.3549 28.37 15.8629 27.549 15.8629 26.614V23.068C15.8629 22.288 16.2099 21.591 16.7489 21.103V15.977C16.7489 15.977 15.6959 8.00001 26.4989 8.00001C37.3019 8.00001 36.249 15.977 36.249 15.977V21.103C36.789 21.591 37.1349 22.288 37.1349 23.068V26.614C37.1349 27.806 36.3349 28.809 35.249 29.144C34.644 31.025 33.771 32.818 32.617 34.448C32.326 34.859 32.0539 35.207 31.8159 35.478V38.8C31.8159 40.023 32.507 41.142 33.601 41.688L42.068 45.921C42.576 46.175 43.035 46.496 43.458 46.853C49.1679 42.091 52.8569 34.971 52.9939 26.953C53.2459 12.32 41.5869 0.254007 26.9529 0.0040072Z" fill="#525252"/>
                        </g>
                        <defs>
                        <clipPath id="clip0">
                        <rect width="53" height="53" fill="white"/>
                        </clipPath>
                        </defs>
                    </svg>
                    <!-- NICKNAME INPUT --> 
                    <input type="text" id="gotoUsername" placeholder="Your nickname" autocomplete="off">
                    
               
                    <!-- GOTO Button --> 
                    <input type="button" id="gotoSend" value="Go to chat room" autocomplete="off">
                </section>
            </section>
        `
        return view
    }, // go to room FORM
    create(){
        let view =  /*html*/`
            <section class="createForm">
                <section id="createForm-inputs" class="entry-inputs">
                    <!-- ROOMNAME ICON --> 
                    <svg id="roomname-icon" width="58" height="58" viewBox="0 0 58 58" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0)">
                        <path d="M29 58C45.0163 58 58 45.0163 58 29C58 12.9837 45.0163 0 29 0C12.9837 0 0 12.9837 0 29C0 45.0163 12.9837 58 29 58Z" fill="#525252"/>
                        <path d="M17.5509 35H14.8909L11.5309 29.12V35H9.0109V25.2H11.6709L15.0309 31.08V25.2H17.5509V35ZM20.9875 35H18.2575L21.9675 25.2H24.7675L28.4775 35H25.7475L25.1175 33.25H21.6175L20.9875 35ZM22.2475 31.43H24.4875L23.3675 28.14L22.2475 31.43ZM37.5769 29.54L35.6169 33.04H33.6569L31.6969 29.54V35H29.1769V25.2H31.8369L34.6369 30.38L37.4369 25.2H40.0969V35H37.5769V29.54ZM49.5524 35H41.7824V25.2H49.5524V27.16H44.3024V29.05H48.4324V31.01H44.3024V33.04H49.5524V35Z" fill="white"/>
                        </g>
                        <defs>
                        <clipPath id="clip0">
                        <rect width="58" height="58" fill="white"/>
                        </clipPath>
                        </defs>
                    </svg>
                    <!-- ROOMNAME INPUT --> 
                    <input type="text" id="createRoomCode" placeholder="Room name">
                    
                    <!-- PERSON/NICKNAME ICON --> 
                    <svg id="person-icon" width="53" height="53" viewBox="0 0 53 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                       <g clip-path="url(#clip0)">
                        <path d="M18.613 41.552L10.706 45.865C10.242 46.118 9.82501 46.429 9.43701 46.768C14.047 50.655 19.998 53 26.5 53C32.954 53 38.867 50.69 43.464 46.856C43.04 46.498 42.58 46.176 42.07 45.922L33.603 41.689C32.509 41.142 31.818 40.024 31.818 38.801V35.479C32.056 35.208 32.328 34.86 32.619 34.449C33.773 32.819 34.646 31.026 35.251 29.145C36.337 28.81 37.137 27.807 37.137 26.615V23.069C37.137 22.289 36.79 21.592 36.251 21.104V15.978C36.251 15.978 37.304 8.00101 26.501 8.00101C15.698 8.00101 16.751 15.978 16.751 15.978V21.104C16.211 21.592 15.865 22.289 15.865 23.069V26.615C15.865 27.549 16.356 28.371 17.091 28.846C17.977 32.703 20.297 35.479 20.297 35.479V38.719C20.296 39.899 19.65 40.986 18.613 41.552Z" fill="#E7ECED"/>
                        <path d="M26.9529 0.0040072C12.3199 -0.245993 0.253949 11.414 0.00394877 26.047C-0.138051 34.344 3.55995 41.801 9.44795 46.76C9.83295 46.424 10.2459 46.116 10.7049 45.866L18.6119 41.553C19.6489 40.987 20.2949 39.9 20.2949 38.718V35.478C20.2949 35.478 17.9739 32.702 17.0889 28.845C16.3549 28.37 15.8629 27.549 15.8629 26.614V23.068C15.8629 22.288 16.2099 21.591 16.7489 21.103V15.977C16.7489 15.977 15.6959 8.00001 26.4989 8.00001C37.3019 8.00001 36.249 15.977 36.249 15.977V21.103C36.789 21.591 37.1349 22.288 37.1349 23.068V26.614C37.1349 27.806 36.3349 28.809 35.249 29.144C34.644 31.025 33.771 32.818 32.617 34.448C32.326 34.859 32.0539 35.207 31.8159 35.478V38.8C31.8159 40.023 32.507 41.142 33.601 41.688L42.068 45.921C42.576 46.175 43.035 46.496 43.458 46.853C49.1679 42.091 52.8569 34.971 52.9939 26.953C53.2459 12.32 41.5869 0.254007 26.9529 0.0040072Z" fill="#525252"/>
                        </g>
                        <defs>
                        <clipPath id="clip0">
                        <rect width="53" height="53" fill="white"/>
                        </clipPath>
                        </defs>
                    </svg>
                    <!-- PERSON/NICKNAME INPUT --> 
                    <input type="text" id="createUsername" placeholder="Your nickname" autocomplete="off">
                    
                    <!-- CREATE BUTTON --> 
                    <input type="button" id="createSend" value="Create chat room" autocomplete="off">
                </section>
            
            </section>
        `
        return view
    }, // create room FORM
    back(){
         return /*html*/`
                <!-- < back icon -->
                <svg id="back-icon" width="13" height="24" viewBox="0 0 13 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 1L2 11.56L12 23" stroke="black" stroke-width="2"/>
                </svg>
                <label for="back-icon">Back</label>
        `
    }, // USED ON MOBILE VERSION ONLY
    desktop(){
        return this.goto() +
            this.create() + `
            <!-- RIGHT MENU OVERLAY -->
            <svg id="rightMenuOverlay" width="712" height="1365" viewBox="0 0 660 1365" fill="none" xmlns="http://www.w3.org/2000/svg">
                 <path d="M190.042 1026C334.047 826.007 487.922 -2 0 -2H790V1026H190.042Z" fill="#363636"/>
                 <text class="thisChatAppSucks" fill="white" x="300" y="820">This chat app sucks!</text>
                 <text class="AnyOtherChatApp" fill="white" x="320" y="840">Any other chat app at 19:20</text>
                 <text class="chatteyIsTypingText" fill="white" x="390" y="900">Chattey is typing</text>
                 <text class="tripleDot" fill="white" x="622" y="900">.
                 <animate attributeName="y" from="890" to="900" dur="1.2s" begin="0s" repeatCount="indefinite"/></text>
                 <text class="tripleDot" fill="white" x="630" y="900">.
                 <animate attributeName="y" from="890" to="900" dur="1.2s" begin="0.2s" repeatCount="indefinite"/></text>
                 <text class="tripleDot" fill="white" x="638" y="900">.
                 <animate attributeName="y" from="890" to="900" dur="1.2s" begin="0.4s" repeatCount="indefinite"/></text>
            </svg>
            `
    } // DESKTOP VERSION ONLY RIGHT OVERLAY
}

export default EntryModule;