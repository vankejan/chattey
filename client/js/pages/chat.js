import Page from "./page.js"
import {onSendMessage, onRecieveMessage, onSendPhotos} from "./handlers/messageHandler.js"
import {onLeaveRoom} from "./handlers/leaveHandler.js"
import chatModule from "./modules/chatModule.js"


class Chat extends Page {
    constructor(){
        super()
        // room properties
        this._code = null;
        this._roomName = null;
        this._userName = null;
        // page elements
        this._chatField = chatModule.chatField()
        this._chatWindow = chatModule.chatWindow()
        this._mobileNav = chatModule.mobileNav()
        this._mobileMenu = chatModule.mobileMenu()
        this._errorLabel = chatModule.errorLabel()
        this._desktopNav = chatModule.desktopMenu()
        this._closeButton = chatModule.closeButton()

        this._imageContainer = document.querySelector(".chatFieldOverLay")
        this._sendImages = []

        // helper properties
        this._mobileMenuOpen = false
        this._linkInput = null

        // easter egg properties
        this._monkey = false
    }

    setView(content){
        this._view =  /*html*/`
            <section class="chat-section">
                 ${content}
                 ${this._chatWindow}
                 ${this._chatField}      
                 ${this._errorLabel}           
            </section>
        `
    }

    async render(code) {
        this._code = code;
        if (mobilecheck()){
            this.setView(this._mobileNav + this._mobileMenu)
        } else{
            this.setView(this._desktopNav);
        }

        return this._view
    }

    after_render() {

        // INITIALIZATION OF THE image container - drag and drop field
        this._imageContainer = document.querySelector(".chatFieldOverLay")

        // mobile menu open handler
        if (mobilecheck()){
            const navButtonOpen = document.querySelector("#openMenuMobile")
            navButtonOpen.addEventListener("click", e => this.toggleMobileMenu(e))
            const navButtonClose = document.querySelector("#closeChatMobileMenu")
            navButtonClose.addEventListener("click", e => this.toggleMobileMenu(e))
        }

        // message recieving and sending handlers
        socket.on('message', (data) => this.handleMessageRecieve(data));
        const sendMessageButton = document.querySelector("#sendMessage")
        sendMessageButton.addEventListener("click",
            e => this.handleMessageSend(e))

        // MENU BUTTONS LISTENERS
        // leave
        const leaveRoomButton = document.querySelector("#leaveRoomChatButton")
        leaveRoomButton.addEventListener("click", evt => this.handlerLeaveRoom(evt, this._code, true))
        // go to
        const gotoRoomButton = document.querySelector("#gotoRoomChatButton")
        gotoRoomButton.addEventListener("click", evt => this.handlerLeaveRoom(evt, this._code, true))
        // create
        const createRoomButton = document.querySelector("#createRoomChatButton")
        createRoomButton.addEventListener("click", evt => this.handlerLeaveRoom(evt, this._code, true))

        // GET LINK BUTTON LISTENER
        const getLinkButton = document.querySelector("#getALinkChatButton")
        getLinkButton.addEventListener("click", evt => {
            let linkInput = document.createElement("input")
            linkInput.setAttribute("type", "text")
            linkInput.setAttribute("class", "inputTextField")
            linkInput.setAttribute("value", this._code)

            if (mobilecheck()){
                if (this._linkInput !== null)
                    document.querySelector(".mobileChatMenu").removeChild(this._linkInput)

                document.querySelector(".mobileChatMenu").appendChild(linkInput)
            } else{
                if (this._linkInput !== null)
                    document.querySelector(".desktopSideMenu").removeChild(this._linkInput)

                document.querySelector(".desktopSideMenu").appendChild(linkInput)
            }
            this._linkInput = linkInput

            navigator.permissions.query({name: "clipboard-write"}).then(result => {
                if (result.state == "granted" || result.state == "prompt") {
                    this.updateClipboard(this._code)
                }
            });
        })

        // GEOLOCATION MESSAGE PARSER
        const field = document.querySelector("#chatField");
        field.addEventListener("input", e => {
            this.geolocationParser(field.value)
        })

        // DRAG AND DROP LISTENERS
        field.addEventListener("drop", e => this.dragAndDropImage(e));
        field.addEventListener("dragover", e => e.preventDefault());

        // SCROLLBAR
        this.scrollbarControl()

        // AUDIO easter eggs
        const moreButton = document.querySelector("#moreMessage")
        moreButton.addEventListener("click", e => {
            if (!this._monkey){
                let audioDiv = document.createElement("div")
                const content = document.querySelector("#main-content")
                audioDiv.innerHTML = chatModule.easterEggMonkeyAudio()

                content.appendChild(audioDiv)
                this._monkey = true
            } else{

            }
        })
    }

    updateClipboard(code) {
        navigator.clipboard.writeText(code).then(function() {
            console.log("Copied room code to the clipboard")
        }, function() {
            /* clipboard write failed */
            console.log("Clipboard write failed")
        });
    }

    handleMessageSend(e){
        // BASIC MESSAGE
        if (this._sendImages.length === 0){
            onSendMessage(
                e,
                this._code,
                document.querySelector("#chatField").value,
                document.querySelector(".errorLabel"),
                document.querySelector("#chatField")
            )
        }
        // PHOTO MESSAGE
        else {
            onSendPhotos(
                e,
                this._code,
                this._sendImages,
                document.querySelector(".errorLabel"),
                document.querySelector("#chatField")
            )
            // RETURN BASIC MESSAGE FIELD TO NORMAL
            this.eraseSendImage(e)
        }

    }

    toggleMobileMenu(e){
        if (this._mobileMenuOpen){
            document.body.classList.add('mobile-menu-visible');
        } else{
            document.body.classList.remove('mobile-menu-visible');
        }
        this._mobileMenuOpen = !this._mobileMenuOpen
    }

    scrollbarControl(){
        const container = document.querySelector('#scrollbar-container')
        const content = document.querySelector('.chatWindowMessages')
        const scroll = document.querySelector('#scrollbar')

        content.addEventListener('scroll', e => {
            scroll.style.height = container.clientHeight * content.clientHeight / content.scrollHeight + "px";
            scroll.style.top = container.clientHeight * content.scrollTop / content.scrollHeight + "px";
        });
        let event = new Event('scroll');

        window.addEventListener('resize', content.dispatchEvent.bind(content, event));
        content.dispatchEvent(event);

        scroll.addEventListener('mousedown', function(start){
            start.preventDefault();
            let y = scroll.offsetTop;
            let onMove = function(end){
                let delta = end.pageY - start.pageY;
                scroll.style.top = Math.min(container.clientHeight - scroll.clientHeight, Math.max(0, y + delta)) + 'px';
                content.scrollTop = (content.scrollHeight * scroll.offsetTop / container.clientHeight);
            };
            document.addEventListener('mousemove', onMove);
            document.addEventListener('mouseup', function(){
                document.removeEventListener('mousemove', onMove);
            });
        });
    }

    handleMessageRecieve(data){
        const content = document.querySelector('.chatWindowMessages')

        let messageObject = onRecieveMessage(data)

        if (messageObject !== null){
            const chatField =  document.querySelector("#chatField")
            if (chatField !== null || chatField !== undefined){
                chatField.value = ""
            }
            document.querySelector(".chatWindowMessages").appendChild(messageObject.getMessageTemplate)
            content.scrollTop = content.scrollHeight;
        }
    }

    handlerLeaveRoom(event, room, redirect){
        socket.emit('leave', room, (err) => onLeaveRoom(err));

        this.toggleMobileMenu(event)

        if (redirect)
            location.hash = '#/' // redirect to front page
    }

    geolocationParser(message){
        if (message.indexOf("{me}") >= 0){

            const field = document.querySelector("#chatField");

            if ("geolocation" in navigator) {
                /* geolocation is available */
                navigator.geolocation.getCurrentPosition(function(position) {
                    console.log(" found current position : {"+ position.coords.latitude + "," + position.coords.longitude + "}")

                    let pos = {"lat": position.coords.latitude, "lon": position.coords.longitude}

                    field.value = JSON.stringify(pos)
                });
            } else {
                /* geolocation IS NOT available */
                console.log("Geologation not available.")
            }
        }
    }

    dragAndDropImage (e) {
        e.preventDefault();
        const files = e.dataTransfer.files;

        e.target.value = ""
        e.target.disabled = true
        e.target.classList.add("dragAndDrop")
        this._imageContainer.classList.add("dragAndDropOverlay")
        this._sendImages.length = 0

        for (const f of files) {
            if (f.type.indexOf("image") !== -1) {
                const fr = new FileReader();
                fr.addEventListener("load", e => {
                    const i = new Image();
                    i.addEventListener("load", e=> {
                        i.width = Math.min (i.naturalWidth, window.innerWidth-20);
                    })
                    i.classList.add("chatImageSend")
                    i.src = fr.result;

                    // add to image array
                    this._sendImages.push(fr.result)

                    // add to the preview
                    this._imageContainer.appendChild(i)
                    console.log("Image loaded")
                })
                fr.readAsDataURL(f);
            }
        }
        // remove photos button
        const close = document.createElement("div")
        close.classList.add("removeSendPhotos")
        close.innerHTML = this._closeButton;
        this._imageContainer.appendChild(close)

        close.addEventListener("click", e => this.eraseSendImage(e))


    }

    eraseSendImage(e){
        this._sendImages.length = 0
        this._imageContainer.classList.remove("dragAndDropOverlay")

        for (let i = 0; i < this._imageContainer.children.length; i++) {
            let child = this._imageContainer.children[i]
            if (child.id !== "chatField"){
                child.remove()
                i = 0;
            } else{
                child.classList.remove("dragAndDrop")
                child.disabled = false
            }
        }
    }
}


export default Chat;