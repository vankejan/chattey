const Utils = {
    // PARSE THE URL
    parseRequestURL : () => {

        let url = location.hash.slice(1).toLowerCase() || '/';
        let r = url.split("/")
        let request = {
            resource    : null,
            code          : null,
            verb        : null
        }
        request.resource    = r[1]
        request.code          = r[2]
        request.verb        = r[3]

        return request
    }
}

export default Utils;