// Router
import Entry         from './pages/entry.js'
import Chat         from './pages/chat.js'
import Error404         from './pages/errors/404.js'
import Utils         from './helpers/utils.js'

// possible routes
const routes = {
    '/'             : new Entry(),
    '/chat/:code'      : new Chat()
};

const router = async () => {

    const content = null || document.querySelector('#main-content');

    let request = Utils.parseRequestURL()

    // Parse the URL including the :code param
    let parsedURL = (request.resource ? '/' + request.resource : '/') + (request.code ? '/:code' : '') + (request.verb ? '/' + request.verb : '')

    // If the parsed URL is not in our list of supported routes, select the 404 page instead
    let page = routes[parsedURL] ? routes[parsedURL] : Error404
    content.innerHTML = await page.render(request.code); // render page content
    page.after_render(); // listeners

}

// Listen on hash change:
window.addEventListener('hashchange', router);

// Listen on page load:
window.addEventListener('load', router);