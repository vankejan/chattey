const Chatroom = require('./Chatroom')
const chatroomTemplates = require('../config/chatrooms')

module.exports = function () {
  // mapping of all available chatrooms
  let chatrooms = new Map(
    chatroomTemplates.map(c => [
      c.name,
      Chatroom(c)
    ])
  )

  function removeClient(client) {
    chatrooms.forEach(c => c.removeUser(client))
  }

  function getChatroomByName(chatroomName) {
    return chatrooms.get(chatroomName)
  }

  function isChatroomAvailableByCode(code) {
    return serializeChatrooms().some(c => c.code === code);
  }

  function createChatRoom(chatroomName) {
      let randomCode = (function  (){
        return Math.random().toString(36).replace('0.', '') ;
      })();

      while (isChatroomAvailableByCode(randomCode)){
          randomCode = (function  (){
              return Math.random().toString(36).replace('0.', '') ;
          })();
      }
      chatrooms.set(
          chatroomName,
          Chatroom({
            name: chatroomName,
            code: randomCode
          })
      );
  }

  function removeRoom(chatroomName) {
    chatrooms.delete(chatroomName);
  }

  function serializeChatrooms() {
    return Array.from(chatrooms.values()).map(c => c.serialize())
  }

  return {
    removeClient,
    getChatroomByName,
    serializeChatrooms,
    createChatRoom,
    removeRoom
  }
}
